//
//  main.m
//  amaysim
//
//  Created by roy orpiano on 26/05/2017.
//  Copyright © 2017 raorpiano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
