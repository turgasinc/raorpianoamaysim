//
//  CustomerInfoTable.m
//  amaysim
//
//  Created by roy orpiano on 26/05/2017.
//  Copyright © 2017 raorpiano. All rights reserved.
//

#import "CustomerInfoTable.h"

@interface CustomerInfoTable () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSDictionary *collectionJSON;
@property (strong, nonatomic) IBOutlet UILabel *customerName;
@property (strong, nonatomic) IBOutlet UILabel *productInfo;
@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UILabel *dataBalance;

@end

@implementation CustomerInfoTable

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = NO;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.estimatedRowHeight = 139.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"collection" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    _collectionJSON = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]];
    
    NSString *title = _collectionJSON[@"data"][@"attributes"][@"title"];
    NSString *firstName = _collectionJSON[@"data"][@"attributes"][@"first-name"];
    NSString *lastName = _collectionJSON[@"data"][@"attributes"][@"last-name"];
    _customerName.text = [NSString stringWithFormat:@"%@ %@ %@", title, firstName, lastName];
    
    NSArray *included = [[NSArray alloc] initWithArray: _collectionJSON[@"included"]];
    
    NSDictionary *subscription;
    NSDictionary *product;
    
    for (NSDictionary *item in included) {
        if ([item[@"type"] isEqualToString:@"subscriptions"])
            subscription = [[NSDictionary alloc] initWithDictionary:item];
        
        if ([item[@"type"] isEqualToString:@"products"])
            product = [[NSDictionary alloc] initWithDictionary:item];
    }
    
    
    NSString *productName = product[@"attributes"][@"name"];
    NSInteger productPrice = [product[@"attributes"][@"price"] integerValue];
    
    _productInfo.text = [NSString stringWithFormat:@"%@", productName];
    
    _price.text = [NSString stringWithFormat:@"Price: %@", [self formatAmount:(int)productPrice]];
    
    int balance = [subscription[@"attributes"][@"included-data-balance"] intValue];
    
    NSString *subscriptionInfo = [NSString stringWithFormat:@"Your subscription remaining data balance: %0.2lfGB", (double)balance/1000];
    
    _dataBalance.text = subscriptionInfo;
}


- (IBAction)logout:(id)sender {
    
    NSMutableArray *vcs = [[NSMutableArray alloc] initWithArray:[self.navigationController viewControllers]];
    NSUInteger count = [vcs count];
    
    [vcs removeObjectAtIndex:count-2];
    
    self.navigationController.viewControllers = vcs;
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (NSString*) formatAmount:(int) amount
{
    
    int cents = amount % 100;
    int dollars = (amount - cents) / 100;
    
    NSString *camount;
    if (cents <=9)
    {
        camount = [NSString stringWithFormat:@"0%d",cents];
    }
    else
    {
        camount = [NSString stringWithFormat:@"%d",cents];
    }
    NSString *t = [NSString stringWithFormat:@"$%d.%@",dollars,camount];
    
    return t;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate 
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}


@end
