//
//  AppDelegate.h
//  amaysim
//
//  Created by roy orpiano on 26/05/2017.
//  Copyright © 2017 raorpiano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSDictionary *collectionJSON;

@end

